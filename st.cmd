
require essioc
require iocmetadata
require lobox

# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet("DEVICENAME", "RFQ-010:RFS-LO-110")
epicsEnvSet("IPADDR", "rfq-lob-007.tn.esss.lu.se")

iocshLoad("$(lobox_DIR)/lobox.iocsh")

pvlistFromInfo("ARCHIVE_THIS", "$(DEVICENAME):ArchiverList")
pvlistFromInfo("SAVRES_THIS", "$(DEVICENAME):SavResList")

